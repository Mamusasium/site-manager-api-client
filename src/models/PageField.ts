export interface PageField {
  _id: string;
  type: string;
  checkFieldType: string;
  text: string | null;
  media: string | null;
}