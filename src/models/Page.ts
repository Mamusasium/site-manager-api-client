import { PageField } from "./PageField";

export interface Page {
  title: string;
  fields: PageField[];
}