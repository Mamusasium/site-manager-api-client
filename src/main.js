"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteManagerApiClient = void 0;
class SiteManagerApiClient {
    constructor(projectId, env = 'prod', referrer = '') {
        this.projectId = projectId;
        this.apiUrl = env === 'prod' || env === 'production' ? 'https://api.portal.websitehafen.de' : 'http://localhost:3000';
        this.referrer = referrer;
    }
    getProject() {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}`, {
                credentials: 'include',
                headers: {
                    'Original-Referer': this.referrer
                }
            });
            const project = yield res.json();
            if (project['logo']) {
                project['logo'] = {
                    thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}?s=thumb`,
                    medium: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}?s=medium`,
                    original: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}`
                };
            }
            if (project['metaImage']) {
                project['metaImage'] = {
                    thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}?s=thumb`,
                    medium: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}?s=medium`,
                    original: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}`
                };
            }
            return project;
        });
    }
    getPage(pageId) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}/pages/${pageId}`, {
                credentials: 'include',
                headers: {
                    'Original-Referer': this.referrer
                }
            });
            const data = yield res.json();
            const fields = data.fields;
            const f = {};
            if (data['metaImage']) {
                data['metaImage'] = {
                    thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}?s=thumb`,
                    medium: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}?s=medium`,
                    original: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}`
                };
            }
            fields.forEach((field) => {
                switch (field['type']) {
                    case 'checkItem':
                        f[field['_id']] = field['items'].map((item) => {
                            return {
                                'checkItemType': item['checkItemType'],
                                'text': item['text']
                            };
                        });
                        break;
                    case 'text':
                    case 'textarea':
                        if (field['maxItems'] <= 1) {
                            f[field['_id']] = field['text'];
                        }
                        else {
                            f[field['_id']] = field['items'].map((item) => {
                                return {
                                    text: item['text']
                                };
                            });
                        }
                        break;
                    case 'pdf':
                    case 'image':
                        if (field['maxItems'] <= 1) {
                            f[field['_id']] = {
                                thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}?s=thumb`,
                                medium: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}?s=medium`,
                                original: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}`
                            };
                        }
                        else {
                            f[field['_id']] = field['items'].map((item) => {
                                return {
                                    media: {
                                        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}?s=thumb`,
                                        medium: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}?s=medium`,
                                        original: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}`
                                    }
                                };
                            });
                        }
                        break;
                    default:
                        break;
                }
            });
            data['fields'] = f;
            return data;
        });
    }
    getBlogCategories() {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}/blog/categories`, {
                credentials: 'include',
                headers: {
                    'Original-Referer': this.referrer
                }
            });
            return yield res.json();
        });
    }
    getBlogPosts(limit = 20, page = 1, category = null) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}/blog/posts?limit=${limit}&page=${page}&category=${category}`, {
                credentials: 'include',
                headers: {
                    'Original-Referer': this.referrer
                }
            });
            let data = yield res.json();
            data.posts = data.posts.map((post) => {
                if (post['image']) {
                    post['image'] = {
                        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}?s=thumb`,
                        medium: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}?s=medium`,
                        original: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}`
                    };
                }
                return post;
            });
            return data;
        });
    }
    getBlogPost(postId) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}/blog/posts/${postId}`, {
                credentials: 'include',
                headers: {
                    'Original-Referer': this.referrer
                }
            });
            const data = yield res.json();
            if (data['image']) {
                data['image'] = {
                    thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}?s=thumb`,
                    medium: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}?s=medium`,
                    original: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}`
                };
            }
            return data;
        });
    }
    sendContactRequest(firstName, lastName, email, subject, message, hp, captchaToken, extraData = null) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(`${this.apiUrl}/wd/${this.projectId}/contactRequest`, {
                credentials: 'include',
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    subject: subject,
                    message: message,
                    hp: hp,
                    captchaToken: captchaToken,
                    extraData: extraData
                })
            });
            return yield res.json();
        });
    }
}
exports.SiteManagerApiClient = SiteManagerApiClient;
