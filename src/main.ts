import {Page} from "./models/Page";

export class SiteManagerApiClient {

  private projectId: string;
  private apiUrl: string;
  private referrer: string;

  constructor(projectId: string, env: 'dev' | 'development' | 'prod' | 'production' = 'prod', referrer: string = '') {
    this.projectId = projectId;
    this.apiUrl = env === 'prod' || env === 'production' ? 'https://api.portal.websitehafen.de' : 'http://localhost:3000';
    this.referrer = referrer;
  }

  public async getProject() {
    const res = await fetch(`${this.apiUrl}/wd/${this.projectId}`, {
      credentials: 'include',
      headers: {
        'Original-Referer': this.referrer
      }
    });
    const project = await res.json();

    if (project['logo']) {
      project['logo'] = {
        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}?s=thumb`,
        medium: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}?s=medium`,
        original: `${this.apiUrl}/wd/${this.projectId}/media/${project['logo']}`
      };
    }

    if (project['metaImage']) {
      project['metaImage'] = {
        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}?s=thumb`,
        medium: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}?s=medium`,
        original: `${this.apiUrl}/wd/${this.projectId}/media/${project['metaImage']}`
      };
    }

    return project;
  }

  public async getPage(pageId: string): Promise<Page[]> {
    const res = await fetch(`${this.apiUrl}/wd/${this.projectId}/pages/${pageId}`, {
      credentials: 'include',
      headers: {
        'Original-Referer': this.referrer
      }
    });
    const data: any = await res.json();
    const fields: any = data.fields;
    const f: any = {};

    if (data['metaImage']) {
      data['metaImage'] = {
        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}?s=thumb`,
        medium: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}?s=medium`,
        original: `${this.apiUrl}/wd/${this.projectId}/media/${data['metaImage']}`
      };
    }

    fields.forEach((field: any) => {
      switch (field['type']) {
        case 'checkItem':
          f[field['_id']] = field['items'].map((item: any) => {
            return {
              'checkItemType': item['checkItemType'],
              'text': item['text']
            }
          });
          break;
        case 'text':
        case 'textarea':
          if (field['maxItems'] <= 1) {
            f[field['_id']] = field['text'];
          } else {
            f[field['_id']] = field['items'].map((item: any) => {
              return {
                text: item['text']
              };
            });
          }
          break;
        case 'pdf':
        case 'image':
          if (field['maxItems'] <= 1) {
            f[field['_id']] = {
              thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}?s=thumb`,
              medium: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}?s=medium`,
              original: `${this.apiUrl}/wd/${this.projectId}/media/${field['media']}`
            };
          } else {
            f[field['_id']] = field['items'].map((item: any) => {
              return {
                media: {
                  thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}?s=thumb`,
                  medium: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}?s=medium`,
                  original: `${this.apiUrl}/wd/${this.projectId}/media/${item['media']}`
                }
              };
            });
          }
          break;
        default:
          break;
      }
    });

    data['fields'] = f;

    return data;
  }

  public async getBlogCategories() {
    const res = await fetch(`${this.apiUrl}/wd/${this.projectId}/blog/categories`, {
      credentials: 'include',
      headers: {
        'Original-Referer': this.referrer
      }
    });

    return await res.json();
  }

  public async getBlogPosts(limit: number = 20, page: number = 1, category: string | null = null) {
    const res = await fetch(`${this.apiUrl}/wd/${this.projectId}/blog/posts?limit=${limit}&page=${page}&category=${category}`, {
      credentials: 'include',
      headers: {
        'Original-Referer': this.referrer
      }
    });
    let data: any = await res.json();

    data.posts = data.posts.map((post: any) => {
      if (post['image']) {
        post['image'] = {
          thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}?s=thumb`,
          medium: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}?s=medium`,
          original: `${this.apiUrl}/wd/${this.projectId}/media/${post['image']}`
        };
      }

      return post;
    });

    return data;
  }

  public async getBlogPost(postId: string) {
    const res = await fetch(`${this.apiUrl}/wd/${this.projectId}/blog/posts/${postId}`, {
      credentials: 'include',
      headers: {
        'Original-Referer': this.referrer
      }
    });
    const data: any = await res.json();

    if (data['image']) {
      data['image'] = {
        thumbnail: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}?s=thumb`,
        medium: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}?s=medium`,
        original: `${this.apiUrl}/wd/${this.projectId}/media/${data['image']}`
      };
    }

    return data;
  }

  public async sendContactRequest(firstName: string,
                                  lastName: string,
                                  email: string,
                                  subject: string,
                                  message: string,
                                  hp: string,
                                  captchaToken: string,
                                  extraData: any = null) {
    const res = await fetch(
        `${this.apiUrl}/wd/${this.projectId}/contactRequest`,
        {
          credentials: 'include',
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            subject: subject,
            message: message,
            hp: hp,
            captchaToken: captchaToken,
            extraData: extraData
          })
        });
    return await res.json();
  }
}